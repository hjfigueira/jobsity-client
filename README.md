# Jobsity - React Client 

Dependencies
- Docker
- docker-compose

Setup

```
docker-compose build
docker-compose up -d
docker-compose exec web npm i
docker-compose exec web npm start
```
