import {config} from "../config";
import axios from 'axios'
import {MessageModel} from "../models/MessageModel";
import {AuthCommand} from "./AuthCommand";

/**
 * Command to create a balance command
 */
export default class Balance extends AuthCommand
{
    /**
     * Method to register the dialog flow, if it exists
     */
    registerDialog()
    {
        this.run(this.request.bind(this));
    }

    /**
     * Request is the last method of the dialog chain, it sends the real request and process its answer
     * @returns {Promise<void>}
     */
    async request()
    {
        let response = await axios({
            'url' : config.host + config.version + 'private/account/balance',
            'method' : 'get',
        });

        this.returnMessage(response, (response, data) =>
        {
            let balance         = data.balance;
            let currencyCode    = data.currency_code;
            this.controller.addMessage(new MessageModel(`Balance current balance is ${balance} ${currencyCode}`, {direction : 'in'}));
        },
        (response, data) =>
        {
            return new MessageModel(response.message, {direction : 'in'});
        });

        this.dispatch();
    }
}
