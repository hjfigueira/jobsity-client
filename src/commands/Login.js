import {BaseCommand} from "./BaseCommand";
import {config} from "../config";
import axios from 'axios'
import {MessageModel} from "../models/MessageModel";

/**
 * Login command
 */
export default class Login extends BaseCommand
{
    constructor(controller)
    {
        super(controller);

        this.email = '';
        this.password = '';
    }

    /**
     * Method to register the dialog flow, if it exists
     */
    registerDialog()
    {
        this.ask(new MessageModel('Enter your email', {direction : 'in'}),  (param) => {
            this.email = param[0];
        });

        this.ask(new MessageModel('Enter your password', {direction : 'in', type : 'mask'}),(param) => {
            this.password = param[0];
        });

        this.run(this.request.bind(this));
    }

    /**
     * Request is the last method of the dialog chain, it sends the real request and process its answer
     * @returns {Promise<void>}
     */
    async request()
    {
        let response = await axios({
            'url' : config.host + config.version + 'public/user/login',
            'method' : 'post',
            'data' : {
                'email' : this.email,
                'password' : this.password,
            }
        });


        this.returnMessage(response, (response, data) =>
        {
            this.controller.setAuth(data.token, data.user);
            let firstName = data.user;
            return new MessageModel(`Welcome ${firstName}`, {direction : 'in'});
        },
        (response, data) =>
        {
            return new MessageModel(response.message, {direction : 'in'});
        });

        this.dispatch();
    }
}
