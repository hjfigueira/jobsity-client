
import {BaseCommand} from "./BaseCommand";

/**
 * Base command for all the methos that require login to be executed
 */
export class AuthCommand extends BaseCommand
{
    /**
     * Controller to test the token
     * @param controller
     */
    constructor(controller)
    {
        super(controller);
        if(!this.isAuthenticated())
        {
            throw new Error('To access this command, the user must be authenticated');
        }

    }

    /**
     * Method to authenticate
     * @returns {boolean}
     */
    isAuthenticated()
    {
        return !!sessionStorage.getItem('token');
    }

}
