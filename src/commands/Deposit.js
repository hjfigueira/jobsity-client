import {config} from "../config";
import axios from 'axios'
import {MessageModel} from "../models/MessageModel";
import {AuthCommand} from "./AuthCommand";

/**
 * Deposit command
 */
export default class Deposit extends AuthCommand
{
    constructor(controller)
    {
        super(controller);

        this.value = '';
        this.currencyCode = '';
    }

    /**
     * Method to register the dialog flow, if it exists
     */
    registerDialog()
    {
        this.ask(new MessageModel('Enter the value (and currency code optionally)', {direction : 'in'}),  (param) => {
            this.value = parseFloat(param[0].replace(/,/g, ''));
            this.currencyCode = param[1];
        });

        this.run(this.request.bind(this));
    }

    /**
     * Request is the last method of the dialog chain, it sends the real request and process its answer
     * @returns {Promise<void>}
     */
    async request()
    {
        let formData = {
            'value' : this.value
        };

        if(!!this.currencyCode){
            formData['currency_code'] = this.currencyCode;
        }

        let response = await axios({
            'url' : config.host + config.version + 'private/transaction/deposit',
            'method' : 'post',
            'data' : formData
        });

        this.returnMessage(response);
        this.dispatch();
    }
}
