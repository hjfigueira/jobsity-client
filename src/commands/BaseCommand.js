/**
 * Base command to be used in the client side
 */
import {MessageModel} from "../models/MessageModel";

/**
 * Base command, as starting point to any toher command
 */
export class BaseCommand
{
    constructor(controller)
    {
        this.controller = controller;
        this.flow = [];
        this.stepCounter = 0;
        this.cacheCallback = null;

        this.registerDialog();
    }

    /**
     * Method to register the dialog, it should be implemented in the child class
     */
    registerDialog()
    {
        throw new Error('This method must be overwritten')
    }

    /**
     * Method to run the commands of a dialog, step by step following user responses
     * @param text
     */
    async step(text)
    {
        try{
            let action = this.flow[this.stepCounter];
            if(action.hasOwnProperty('text')){
                this.controller.addMessage(action.text);
            }

            if(this.cacheCallback == null){
                this.cacheCallback = action.callback;
            }else{
                await this.cacheCallback(text);
                this.cacheCallback = action.callback;
            }

            if(this.flow.length === this.stepCounter + 1){
                await this.cacheCallback(text);
            }

            this.stepCounter++;
        }
        catch (e) {
            this.dispatch();
            throw e;
        }
    }

    /**
     * Method to prepare a message to be send and a callback to run on the user's response
     * @param text
     * @param callback
     */
    ask(text, callback)
    {
        this.flow.push({ text : text, callback : callback });
    }

    /**
     * Like the ask method, but without sending text to the user
     * @param callback
     */
    run(callback)
    {
        this.flow.push({ callback : callback });
    }

    /**
     * Finishes and clears the command running
     */
    dispatch()
    {
        this.controller.currentCommand = null;
    }

    /**
     * Generic method to process method returns
     * @param response
     * @param success
     * @param error
     */
    returnMessage(response, success = () => {}, error = () => {})
    {
        let data = response.data;
        let message = new MessageModel(data.message, {direction : 'in'});

        if(!data.error){
            let newMessage = success(data, data.data);
            if(!!newMessage){ message = newMessage}
        }else{
            let newMessage = error(data, data.data);
            if(!!newMessage){ message = newMessage}

            if(!!data.validation){
                message.list = data.validation;
            }
        }
        console.log(message);
        this.controller.addMessage(message);
    }
}
