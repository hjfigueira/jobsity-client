import {BaseCommand} from "./BaseCommand";
import {config} from "../config";
import axios from 'axios'
import {MessageModel} from "../models/MessageModel";

/**
 * Signup command
 */
export default class Login extends BaseCommand
{
    constructor(controller)
    {
        super(controller);

        this.name = '';
        this.email = '';
        this.password = '';
        this.passwordConfirmation = '';
        this.baseCurrency = '';
    }

    /**
     * Method to register the dialog flow, if it exists
     */
    registerDialog()
    {
        this.ask(new MessageModel('Enter your complete name', {direction : 'in'}),  (param) => {
            this.name = param.join(' ');
        });

        this.ask(new MessageModel('Enter your email', {direction : 'in'}),  (param) => {
            this.email = param[0];
        });

        this.ask(new MessageModel('Enter your password', {direction : 'in', type : 'mask'}),(param) => {
            this.password = param[0];
        });

        this.ask(new MessageModel('Confirm your password', {direction : 'in', type : 'mask'}),(param) => {
            this.passwordConfirmation = param[0];
        });

        this.ask(new MessageModel('Enter the base currency of your account (USD, EUR, BRL, etc.)', {direction : 'in', type : 'mask'}),(param) => {
            this.baseCurrency = param[0];
        });

        this.run(this.request.bind(this));
    }

    /**
     * Request is the last method of the dialog chain, it sends the real request and process its answer
     * @returns {Promise<void>}
     */
    async request()
    {
        let response = await axios({
            'url' : config.host + config.version + 'public/user/signup',
            'method' : 'post',
            'data' : {
                "name"                  : this.name,
                "email"                 : this.email,
                "currency_code"         : this.baseCurrency,
                "password"              : this.password,
                "password_confirmation" : this.passwordConfirmation
            }
        });

        this.returnMessage(response, (response, data) =>
        {
            this.controller.setAuth(data.token, data.user);
            let firstName = data.user;
            return new MessageModel(`Welcome ${firstName}`, {direction : 'in'});
        },
        (response, data) =>
        {
            return new MessageModel(response.message, {direction : 'in'});
        });

        this.dispatch();
    }
}
