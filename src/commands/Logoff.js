import {config} from "../config";
import axios from 'axios'
import {AuthCommand} from "./AuthCommand";

/**
 * Logoff command
 */
export default class Logoff extends AuthCommand
{
    constructor(controller)
    {
        super(controller);

        this.email = '';
        this.password = '';
    }

    /**
     * Method to register the dialog flow, if it exists
     */
    registerDialog()
    {
        this.run(this.request.bind(this));
    }

    /**
     * Request is the last method of the dialog chain, it sends the real request and process its answer
     * @returns {Promise<void>}
     */
    async request()
    {
        let response = await axios({
            'url' : config.host + config.version + 'private/user/logoff',
            'method' : 'delete'
        });

        this.returnMessage(response, (response, data) => {
            this.controller.unsetAuth(data.token, data.user);
        });
        this.dispatch();
    }
}
