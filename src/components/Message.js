import React from 'react';
import './Message.scss'


export class Message extends React.Component{

    render() {

        let message = this.props.message;

        const list = <ul>
            { Object.values(message.list).map( (item, index) =>
                <li key={index}>{item}</li>
            )}
        </ul>;

        return <div className="message-line" >

            <div className={ `message -${message.data.direction}` } >
                <div className="content">
                    {message.content}
                    {list}
                </div>
            </div>
        </div>;
    }

}
