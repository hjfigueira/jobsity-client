import './Chat.scss'
import React from 'react';
import {Header} from "./Header";
import {MessagesList} from "./MessagesList";
import {Footer} from "./Footer";
import {ChatController} from "../controllers/ChatController";
import axios from "axios"

/**
 * Main class for the component definition
 */
export class Chat extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            auth : {}
        };

        this.sessionStorage = window.sessionStorage;
        this.chatController = new ChatController(this);
    }

    render = () =>
    {
        return <div className="chat">
            <Header user={this.state.auth.user}/>
            <MessagesList ref={ list => {this.list = list} }/>
            <Footer onUserSend={this.userSend} />
        </div>;
    }

    /**
     * When component is mounted
     * @returns {Promise<void>}
     */
    componentDidMount = async () =>
    {
        this.chatController.greetings();

        let user    = this.sessionStorage.getItem('user');
        let token   = this.sessionStorage.getItem('token');
        if(!!user && !!token){
            this.setAuth(token, user);
        }
    }

    /**
     * Event when user sends a message
     * @param messageModel
     * @returns {Promise<void>}
     */
    userSend = async (messageModel) =>
    {
        this.list.addMessage(messageModel, 'out');
        this.chatController.invoke(messageModel.content);
    }

    /**
     * Method to set the auth token
     * @param token
     * @param user
     */
    setAuth = (token, user) =>
    {
        this.sessionStorage.setItem('user', user);
        this.sessionStorage.setItem('token', token);

        this.setState({
            'auth' : { 'token' : token, 'user' : user }
        })

        axios.defaults.headers.common = {
            "X-Auth-Token": token,
        };
    }

    /**
     * Method to destroy the user session
     */
    unsetAuth = () =>
    {
        this.sessionStorage.setItem('user', '');
        this.sessionStorage.setItem('token', '');

        this.setState({
            'auth' : { 'token' : null, 'user' : null }
        })
    }
}
