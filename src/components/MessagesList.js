import React from 'react';
import {Message} from "./Message";
import './MessagesList.scss'

export class MessagesList extends React.Component{

    constructor(props)
    {
        super(props);

        this.messageList = {};
        this.state = {
            messages : []
        };
    }

    render = () => {

        return <div className="messages" ref={ el => this.messageList = el }>

            { this.state.messages.map( (message, index) =>
                <Message key={index} message={message} />
            ) }

        </div>;
    }

    /**
     * Adds a new message to the list
     * @param messageModel
     */
    addMessage = (messageModel) =>
    {
        this.state.messages.push(messageModel);

        this.setState({
            messages: this.state.messages
        })
    }

    /**
     * At any time when the component changes
     * @param prevProps
     * @param prevState
     * @param snapshot
     */
    componentDidUpdate(prevProps, prevState, snapshot)
    {
        this._scrollToBottom();
    }

    /**
     * Move scrool
     * @private
     */
    _scrollToBottom = () =>
    {
        let list = this.messageList;
        list.scrollTop = list.scrollHeight;
    }

}
