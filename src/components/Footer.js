import React from 'react';
import './Footer.scss'
import icon from './../static/icons/send.svg'
import {MessageModel} from "../models/MessageModel";

/**
 * Footer component, it contains the area for user interactions
 */
export class Footer extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            text : '',
            inputType : 'text'
        };
    }

    render = () => {
        return <div className="footer">

            <input type={this.state.inputType} className="textInput" placeholder="Press enter to send ..." value={this.state.text} onKeyUp={this.onEnterPress} onChange={ event => this.setMessage(event)}/>

            <button className="sendButton" onClick={this.onClickSend}>
                <img alt='Send message' className="iconSend" src={icon}/>
            </button>

        </div>;
    }

    /**
     * Method for updating the text value
     * @param event Dom Event data
     */
    setMessage = (event) => {
        this.setState({ text : event.target.value })
    }

    /**
     * In case that the user decides to press enter
     * @param event Dom Event data
     */
    onEnterPress = (event) =>{

        if(event.key === 'Enter'){
            this.sendMessage();
        }
    }

    /**
     * In case that the user decides to click on the button, mainly for mobile
     */
    onClickSend = () => {
        this.sendMessage()
    }

    /**
     * Method that actually triggers the send event
     */
    sendMessage = () => {

        if(this.state.text === ''){ return; }

        if(this.props.hasOwnProperty('onUserSend')){
            this.props.onUserSend(new MessageModel(this.state.text, { direction : 'out' }));
            this.setState({text : ''});
        }
    }
}
