import React from 'react';
import './Header.scss'
import icon from '../static/icons/face.svg'

export class Header extends React.Component
{

    render() {

        let partHeader = '';
        let loginClass = '';

        if(!!this.props.user)
        {
            loginClass = 'logged';
            partHeader =  <div className="extra-info">
                <div className="status">Logged in as</div>
                <div className="name">{this.props.user}</div>
            </div>
        }
        else
        {
            partHeader =  <div className="extra-info waiting">
                <div className="status">Waiting login</div>
                <div className="name">Anonymous user</div>
            </div>
        }


        return <div className={ 'header ' + loginClass}>

            <div className="panel-left">
                <div className="portrait">
                    <img src={icon} alt="User portrait" />
                </div>
                {partHeader}
            </div>

        </div>;
    }

}
