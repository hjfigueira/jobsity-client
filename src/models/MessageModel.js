
export class MessageModel {

    constructor(content, data)
    {
        this.data       = data;
        this.content    = content;
        this.date       = new Date();
        this.list       = {};
    }

}
