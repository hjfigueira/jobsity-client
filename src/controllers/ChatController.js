import {BaseController} from "./BaseController";
import {MessageModel} from "../models/MessageModel";

export class ChatController extends BaseController{

    constructor(reactComponent)
    {
        super(reactComponent);
        this.commands = {};
        this.currentCommand = null;

        this.register();
    }

    register()
    {
        this.commands = {
            'login'     : import('../commands/Login'),
            'logoff'    : import('../commands/Logoff'),
            'signup'    : import('../commands/Signup'),
            'deposit'   : import('../commands/Deposit'),
            'withdraw'  : import('../commands/Withdraw'),
            'balance'   : import('../commands/Balance'),
        };
    }

    greetings()
    {
        this.addMessage(new MessageModel('Hi user, first to access your account, login or signup',{ 'direction' : 'in' }));
    }

    undefinedCommand(text)
    {
        this.addMessage(new MessageModel(`The command ${text} could not be executed`,{ 'direction' : 'in' }));
    }

    exceptionMessage(error)
    {
        this.addMessage(new MessageModel(`${error.message}`,{ 'direction' : 'in' }));
    }

    async invoke(text)
    {
        try
        {
            let params = text.split(' ');
            let commands = params.shift();

            let command = this.commands[commands];
            if (!!command && this.currentCommand === null)
            {
                let module = await command;
                this.currentCommand = new module.default(this);
                await this.currentCommand.step(params);
            } else if (this.currentCommand !== null)
            {
                let params = text.split(' ');
                await this.currentCommand.step(params);
            } else
            {
                this.undefinedCommand(text);
            }
        }
        catch (error) {

            if(error.response && error.response.status === 401){
                this.unsetAuth();
                this.exceptionMessage(error.response.data)
            }else{
                this.exceptionMessage(error)
            }
        }

    }

    setAuth(token, user)
    {
        this.reactComponent.setAuth(token, user);
    }

    unsetAuth()
    {
        this.reactComponent.unsetAuth();
    }

}
