
export class BaseController{

    constructor(reactComponent)
    {
        this.reactComponent = reactComponent;
    }

    addMessage(messageModel)
    {
        return this.reactComponent.list.addMessage(messageModel);
    }
}
